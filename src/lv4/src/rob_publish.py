#!/usr/bin/env python
import rospy
import tf
from geometry_msgs.msg import TransformStamped
from sensor_msgs.msg import Image
from tf.transformations import euler_from_quaternion
from math import sqrt, pi
from cv_bridge import CvBridge


rospy.init_node('rob_publish')

transformStamped = TransformStamped()
listener = tf.TransformListener()
distance = 0.0
rotation = 1.0
x_trans_prev = 8.0
y_trans_prev = -8.0
z_rot_prev = 0.0
image = Image()
bridge = CvBridge()

def imageCallback(msg):
    global image
    image = msg

rob_tf_pub = rospy.Publisher("/polozaj",TransformStamped,queue_size=10)
image_sub = rospy.Subscriber("/camera/image_raw", Image, imageCallback)
image_pub = rospy.Publisher("/slike", Image, queue_size=10)

rate = rospy.Rate(10)
while not rospy.is_shutdown():
    try:
        trans, rot = listener.lookupTransform('map','base_link', rospy.Time.now())
        transformStamped.header.stamp = rospy.Time.now()
        transformStamped.header.frame_id = 'map'
        transformStamped.child_frame_id = 'base_link'
        transformStamped.transform.translation.x = trans[0]
        transformStamped.transform.translation.y = trans[1]
        transformStamped.transform.translation.z = 0.0
        transformStamped.transform.rotation.x = 0.0
        transformStamped.transform.rotation.y = 0.0
        transformStamped.transform.rotation.z = rot[2]
        transformStamped.transform.rotation.w = rot[3]
        
        e = euler_from_quaternion([0, 0, rot[2], rot[3]])
        distance = sqrt((trans[0]-x_trans_prev)**2 + (trans[1]-y_trans_prev)**2)
        angle_diff = abs(e[2] - z_rot_prev)*180 / pi   
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
        continue

    rob_tf_pub.publish(transformStamped)
    if distance > 2.0 or angle_diff > 60:
        image_pub.publish(image)
        x_trans_prev = trans[0]
        y_trans_prev = trans[1]
        z_rot_prev = e[2]
        
    rate.sleep
