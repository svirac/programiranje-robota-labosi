#!/usr/bin/env python
from sys import path
import rosbag
import Tkinter as tk
import numpy as np
from math import sqrt
from cv_bridge import CvBridge
import cv2 as cv
from PIL import Image, ImageTk
import random

#bag = rosbag.Bag('/home/user/labosi_ws/src/lv4/bagfiles/path_2021-07-07-11-20-19.bag')
bag = rosbag.Bag('/home/damian/labosi_ws/src/lv4/bagfiles/path_2021-07-07-11-20-19.bag')

root = tk.Tk()
root.title("Draw trajectory")
canvas = tk.Canvas(root, height=951, width=1161)
canvas.pack(expand=1, fill=tk.BOTH)
img = tk.PhotoImage(file='/home/damian/labosi_ws/src/lv4/maps/willowgarage_refined.pgm')
canvas.create_image(20,20, anchor="nw", image=img)

center_x = 400
center_y = 520
scale = 20

key_points = np.empty((0,2), float) # (x,y)
path_points = np.empty((0,3), float) # (x,y,stamp)
img_stamps = []

bridge = CvBridge()
scale_percent = 5 # percent of original size
width = int(800 * scale_percent / 100)
height = int(800 * scale_percent / 100)
dim = (width, height)
tk_images = []

for topic, msg, t in bag.read_messages(topics=['/key_points', '/polozaj', '/slike']):
    # Get and draw key_points
    if topic == '/key_points' and key_points.size == 0:
        for pose in msg.poses:
            u = center_x + (pose.position.x*scale)
            if pose.position.y <= 0:
                v = center_y + (abs(pose.position.y)*scale)
            else:
                v = center_y + (-pose.position.y*scale)
            canvas.create_rectangle(u-6, v-6, u+6, v+6, outline='black', fill='black')
            new_point = np.array([[pose.position.x, pose.position.y]])
            key_points = np.concatenate((key_points, new_point), axis=0)

    # Get and draw trajectory
    elif topic == '/polozaj':
        u = center_x + (msg.transform.translation.x*scale)
        if msg.transform.translation.y <= 0:
            v = center_y + (abs(msg.transform.translation.y)*scale)
        else:
            v = center_y + (-msg.transform.translation.y*scale)
        canvas.create_oval(u-1, v-1, u+1, v+1, outline='red', fill='red')
        new_point = np.array([[msg.transform.translation.x, msg.transform.translation.y, float(msg.header.stamp.secs) + float(msg.header.stamp.nsecs)/1000000000]])
        path_points = np.concatenate((path_points, new_point), axis=0)

    # Get images
    elif topic == '/slike':
        img_stamps.append(float(msg.header.stamp.secs) + float(msg.header.stamp.nsecs)/1000000000)
        cv_image = bridge.imgmsg_to_cv2(msg)
        cv_image = cv.resize(cv_image, dim, interpolation = cv.INTER_AREA)
        b,g,r = cv.split(cv_image)
        rgb_image = cv.merge((r,g,b))
        image_x = Image.fromarray(rgb_image)
        img_tk = ImageTk.PhotoImage(image=image_x)
        tk_images.append(img_tk)

# Assign robot position to each image
img_points = np.empty((0,4), float) # (x,y,stamp,img)
for i, img_stamp in enumerate(img_stamps):
    for path_point in path_points:
        time_diff = abs(path_point[2] - img_stamp)
        if time_diff < 0.1:
            new_point = np.array([[path_point[0], path_point[1], img_stamp, tk_images[i]]])
            img_points = np.concatenate((img_points, new_point), axis=0)
            break

# Draw image function
flag = True
def draw_image(img):
    global flag
    u = center_x + (img_point[0]*scale)
    if img_point[1] <= 0:
        v = center_y + (abs(img_point[1])*scale)
    else:
        v = center_y + (-img_point[1]*scale)

    rand_u = random.randint(30, 80)
    rand_v = random.randint(20, 60)
    if flag:
        u_img = u - rand_u
        v_img = v + rand_v
    else:
        u_img = u + rand_u
        v_img = v - rand_v
    item = canvas.create_image(u_img,v_img, image=img)
    canvas.create_line(u_img, v_img, u, v, arrow=tk.LAST, fill="blue")
    flag = not flag
    return

# Find closest 3 images to key_points and draw them
for key_point in key_points:
    img_count = 0
    for img_point in img_points:
        distance = sqrt((img_point[0]-key_point[0])**2 + (img_point[1]-key_point[1])**2)
        if distance <= 2.0 and img_count < 3:
            draw_image(img_point[3])
            img_count += 1
        elif distance > 2.0 and img_count > 0 and img_count < 3:
            draw_image(img_point[3])
            img_count += 1
        elif img_count >= 3:
            break

# Draw last image
draw_image(img_points[9][3])

bag.close()
root.mainloop()
