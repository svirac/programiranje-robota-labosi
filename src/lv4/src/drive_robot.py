#!/usr/bin/env python
import rospy
import copy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseArray

rospy.init_node('drive_robot')
key_points_pub = rospy.Publisher("/key_points", PoseArray, queue_size=1)
client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
client.wait_for_server()

goal = MoveBaseGoal()
goals = []
poseArray = PoseArray()


# Pose 1
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = 12.199136734
goal.target_pose.pose.position.y = -10.7338886261
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = -0.408383069533
goal.target_pose.pose.orientation.w = 0.999987505316
goals.append(copy.deepcopy(goal))

# Pose 2
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = 14.9870424271
goal.target_pose.pose.position.y = -14.7333612442
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = -0.721975303816
goal.target_pose.pose.orientation.w = 0.69191882521
goals.append(copy.deepcopy(goal))

# Pose 3
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = 12.0042572021
goal.target_pose.pose.position.y = -16.3888664246
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.999992852341
goal.target_pose.pose.orientation.w = -0.00378090831705
goals.append(copy.deepcopy(goal))

# Pose 4
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = -8.25410461426
goal.target_pose.pose.position.y = -16.0133342743
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.999968339942
goal.target_pose.pose.orientation.w = -0.00795733080712
goals.append(copy.deepcopy(goal))

# Pose 5
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = -9.8666973114
goal.target_pose.pose.position.y = -14.2523975372
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.694400513582
goal.target_pose.pose.orientation.w = 0.719588720546
goals.append(copy.deepcopy(goal))

# Pose 6
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = -9.86625957489
goal.target_pose.pose.position.y = -6.00992012024
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.924038255303
goal.target_pose.pose.orientation.w = 0.382300016659
goals.append(copy.deepcopy(goal))

# Pose 7
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = -13.5301647186
goal.target_pose.pose.position.y = -1.89379119873
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.698427815337
goal.target_pose.pose.orientation.w = 0.715680506066
goals.append(copy.deepcopy(goal))

# Pose 8
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = -13.2261180878
goal.target_pose.pose.position.y = 8.54953765869
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = -0.0241251834917
goal.target_pose.pose.orientation.w = 0.999708945404
goals.append(copy.deepcopy(goal))

# Pose 9
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = 3.64224863052
goal.target_pose.pose.position.y = 7.85420799255
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = -0.00397492177854
goal.target_pose.pose.orientation.w = 0.999992099967
goals.append(copy.deepcopy(goal))

# Pose 10
goal.target_pose.header.frame_id ='map' #reference frame name
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = 13.217625618
goal.target_pose.pose.position.y = 8.08989524841
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.704158920176
goal.target_pose.pose.orientation.w = 0.71004240376
goals.append(copy.deepcopy(goal))

for goal in goals:
    poseArray.header = goal.target_pose.header
    poseArray.poses.append(goal.target_pose.pose)

for goal in goals:
    key_points_pub.publish(poseArray)
    client.send_goal(goal)
    wait = client.wait_for_result()
