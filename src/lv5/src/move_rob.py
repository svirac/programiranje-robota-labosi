#!/usr/bin/env python

import copy
import rospy
import moveit_commander
from geometry_msgs.msg import Pose
from tf.transformations import quaternion_from_euler
from std_msgs.msg import Bool

rospy.init_node("move_rob_node")
start_drawing_pub = rospy.Publisher("/start_drawing",Bool,queue_size=10)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "arm"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Move robot to drawing pose
drawing_pose = Pose()
drawing_pose.position.x = 0.16
drawing_pose.position.y = -0.34
drawing_pose.position.z = 0.11
q = quaternion_from_euler(0.0, 3.14, 0.0)
drawing_pose.orientation.x = q[0]
drawing_pose.orientation.y = q[1]
drawing_pose.orientation.z = q[2]
drawing_pose.orientation.w = q[3]

move_group.set_pose_target(drawing_pose, end_effector_link="hand")
plan = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

# Start drawing
start_drawing_pub.publish(True)

waypoints = []
# pose 1
wpose = move_group.get_current_pose().pose
wpose_1 = copy.deepcopy(wpose)
# pose 2
wpose.position.x += 0.2
wpose_2 = copy.deepcopy(wpose)
waypoints.append(wpose_2)
# pose 3
wpose.position.y += 0.2
wpose_3 = copy.deepcopy(wpose)
waypoints.append(wpose_3)
# pose 4
wpose.position.x -= 0.2
wpose_4 = copy.deepcopy(wpose)
waypoints.append(wpose_4)
# pose 5
wpose.position.x += 0.1
wpose.position.y += 0.1
waypoints.append(copy.deepcopy(wpose))
# pose 6 = pose 3
# pose 7 = pose 1
# pose 8 = pose 4
# pose 9 = pose 2
waypoints.extend([wpose_3, wpose_1, wpose_4, wpose_2])

(plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
move_group.execute(plan, wait=True)
move_group.stop()
move_group.clear_pose_targets()

