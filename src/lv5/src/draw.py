#!/usr/bin/env python

import math
import rospy
from std_msgs.msg import Bool
from sensor_msgs.msg import PointCloud
from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Point32


point_cloud = PointCloud()
first_msg = True
points = Point32()

def link_states_callback(msg):
    global point_cloud
    global first_msg
    global points

    msg_x = msg.pose[8].position.x
    msg_y = msg.pose[8].position.y
    msg_z = msg.pose[8].position.z

    if first_msg:
        point_cloud.header.frame_id = "world"
        point_cloud.points.append(msg.pose[8].position)
        points.x = msg_x
        points.y = msg_y
        points.z = msg_z
        first_msg = False
    else:
        e_dist = math.sqrt((points.x-msg_x)**2 + (points.y-msg_y)**2 + (points.z-msg_z)**2)
        if (e_dist > 0.01):
            point_cloud.header.frame_id = "world"
            point_cloud.points.append(msg.pose[8].position)
            points.x = msg_x
            points.y = msg_y
            points.z = msg_z
    

rospy.init_node("draw_node")
rospy.wait_for_message("/start_drawing", Bool)
path_sub = rospy.Subscriber("/gazebo/link_states",LinkStates,link_states_callback)
points_pub = rospy.Publisher("/drawing_points",PointCloud,queue_size=10)

rate = rospy.Rate(10.0)
while not rospy.is_shutdown():
    points_pub.publish(point_cloud)
    rate.sleep()


