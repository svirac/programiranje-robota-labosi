#!/usr/bin/env python

import rospy
from Tkinter import *
from lv2.msg import Znamenitost
from turtlesim.msg import Pose


msgZnamenitost = Znamenitost()

def poseCallback(msg):
    global msgZnamenitost
    msgZnamenitost.x = msg.x
    msgZnamenitost.y = msg.y
    msgZnamenitost.theta = msg.theta


rospy.init_node("znamenitost_pub")
pub = rospy.Publisher("/znamenitost", Znamenitost, queue_size="10")
sub = rospy.Subscriber("/turtle1/pose", Pose, poseCallback)

pub_mode = rospy.get_param("~publisher_mode","1")   # 0-user mode / 1-periodic mode
pub_rate = rospy.get_param("~publisher_freq","0.5") 

if pub_mode == 1:
    rate = rospy.Rate(float(pub_rate))
    br = 0
    while not rospy.is_shutdown():
        msgZnamenitost.name = "Znamenitost_" + str(br)
        pub.publish(msgZnamenitost)
        br += 1
        rate.sleep()       
else:
    root = Tk()
    root.title("Znamenitost Publisher")
    root.geometry("300x50")
    lbl = Label(root, text="Oznaka")
    lbl.grid(column=0, row=0)
    txt = Entry(root, width=20)
    txt.grid(column=1, row=0)
    def clicked():
        msgZnamenitost.name = txt.get()
        pub.publish(msgZnamenitost)
    btn = Button(root, text="Posalji", command=clicked)
    btn.grid(column=2, row=0)

    root.mainloop()


