#!/usr/bin/env python

import rospy
from lv2.msg import Znamenitost
from turtlesim.msg import Pose
from Tkinter import *


pose = Pose()
znamenitost = Znamenitost()
scale = 30

def poseCallback(msg):
    global canvas
    global scale
    x =  msg.x*scale
    y =  400-msg.y*scale
    print (x,y)
    canvas.create_oval(x, y, x+1, y+1)

def znmCallback(msg):
    global canvas
    global scale
    x =  msg.x*scale
    y =  400-msg.y*scale
    canvas.create_rectangle(x-3, y-3, x+3, y+3, fill="red")
    canvas.create_text(x+55, y-10, text=msg.name)

rospy.init_node("draw_from_bagfile")
sub_pose = rospy.Subscriber("/turtle1/pose", Pose, poseCallback)
sub_znm = rospy.Subscriber("/znamenitost", Znamenitost, znmCallback)

root = Tk()
root.title("Draw trajectory")
root.geometry("400x500")
canvas = Canvas(root, height=400, width=500)
canvas.pack()

root.mainloop()



