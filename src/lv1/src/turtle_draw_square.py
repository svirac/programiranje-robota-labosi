#!/usr/bin/env python

import rospy
import time
from geometry_msgs.msg import Twist


rospy.init_node('turtle_draw_square')
pub = rospy.Publisher('/turtle1/cmd_vel',Twist,queue_size=1)

msg = Twist()

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

for i in range(7):
    if i%2 == 0:
        msg.linear.x = 3.0
        msg.angular.z = 0.0
    else:
        msg.linear.x = 0.0
        msg.angular.z = -3.14 / 2

    pub.publish(msg)
    time.sleep(1.5)
