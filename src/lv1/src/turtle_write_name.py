#!/usr/bin/env python

import rospy
import time
from geometry_msgs.msg import Twist
from turtlesim.srv import TeleportAbsolute
from std_srvs.srv import Empty
from math import pi


rospy.init_node('turtle_draw_square')
pub = rospy.Publisher('/turtle1/cmd_vel',Twist,queue_size=1)

# Teleport
rospy.wait_for_service('/turtle1/teleport_absolute')
teleport = rospy.ServiceProxy('/turtle1/teleport_absolute',TeleportAbsolute)
location = teleport(0.5, 4.0, pi/2)
time.sleep(0.5)

# Clear
rospy.wait_for_service('/clear')
clear = rospy.ServiceProxy('/clear',Empty)
clc = clear()
time.sleep(0.5)

deg = pi/180
msg = Twist()

# Draw: D
msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -90 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 5.0
msg.angular.z = -180 * deg
pub.publish(msg)
time.sleep(1.5)

# Draw: A
msg.linear.x = 0.0
msg.angular.z = 180 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 70 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -140 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -110 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 180 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -70 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

# Draw: M
msg.linear.x = 0.0
msg.angular.z = 70 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.3
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 90 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -150 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 120 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -149 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

# Draw: I
msg.linear.x = 0.0
msg.angular.z = 90 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.6
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 90 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 180 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

# Draw: A
msg.linear.x = 0.0
msg.angular.z = 90 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.3
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 70 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -140 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -110 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 180 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -70 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 1.5
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

# Draw: N
msg.linear.x = 0.0
msg.angular.z = 70 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.3
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 90 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = -160 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.2
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 0.0
msg.angular.z = 160 * deg
pub.publish(msg)
time.sleep(1.5)

msg.linear.x = 3.0
msg.angular.z = 0.0
pub.publish(msg)
time.sleep(1.5)








