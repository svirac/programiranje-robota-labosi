#!/usr/bin/env python

import rospy
import tf

rospy.init_node("map_odom_tf_node")
br = tf.TransformBroadcaster()
translation = (0.0, 0.0, 0.0) #(x, y, z)
rotation = (0.0, 0.0, 0.0, 1.0) #quaternion (x, y, z, w)
#sendTransform najbolje postaviti u neku petlju da svi uspiju registrirati transformaciju
rate = rospy.Rate(10.0)
while not rospy.is_shutdown():
    br.sendTransform(translation, rotation, rospy.Time.now(), 'odom', 'map') #child, parent
    rate.sleep

