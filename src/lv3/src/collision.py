#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from lv3.msg import Collision

collision = Collision()

def scan_callback(msg):
    global collision
    min_range = min(msg.ranges)
    if min_range < user_min_range:
        collision.collision = True
        min_index = msg.ranges.index(min_range)
        if min_index > 180 and min_index < 540:
            collision.front_collision = True
        else:
            collision.front_collision = False
    else:
        collision.collision = False
        collision.front_collision = False


rospy.init_node("collision_node")
user_min_range = rospy.get_param("~collision_min_range","0.6")
sub_scan = rospy.Subscriber("/scan",LaserScan,scan_callback)
pub_collision = rospy.Publisher("/collision",Collision,queue_size=1)

rate = rospy.Rate(10.0)
while not rospy.is_shutdown():
    pub_collision.publish(collision)
    rate.sleep()
