#!/usr/bin/env python

import rospy
import time
from math import atan2, pi, sqrt, acos
from geometry_msgs.msg import Twist, PoseStamped
from nav_msgs.msg import Odometry
from lv3.msg import Collision
from tf.transformations import euler_from_quaternion


robot_pose = PoseStamped()
goal_pose = PoseStamped()
collision = False

def robot_pose_callback(msg):
    global robot_pose
    robot_pose.pose.position.x = msg.pose.pose.position.x
    robot_pose.pose.position.y = msg.pose.pose.position.y
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (_,_,robot_pose.pose.orientation.z) = euler_from_quaternion(orientation_list)

def goal_pose_callback(msg):
    global goal_pose
    goal_pose.pose.position.x = msg.pose.position.x
    goal_pose.pose.position.y = msg.pose.position.y
    orientation_q = msg.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (_,_,goal_pose.pose.orientation.z) = euler_from_quaternion(orientation_list)

def collision_callback(msg):
    global collision
    collision = msg.front_collision


rospy.init_node("mob_rob_navig_node")
sub_robot_pose = rospy.Subscriber("/odom",Odometry,robot_pose_callback)
sub_goal_pose = rospy.Subscriber("/goal",PoseStamped,goal_pose_callback)
sub_collision = rospy.Subscriber("/collision",Collision, collision_callback)
pub_twist = rospy.Publisher("/cmd_vel",Twist,queue_size=10)
twist_msg = Twist()

while not rospy.is_shutdown():
    rospy.wait_for_message("/goal",PoseStamped)

    delta_x = goal_pose.pose.position.x-robot_pose.pose.position.x
    delta_y = goal_pose.pose.position.y-robot_pose.pose.position.y
    angle_to_goal = atan2(delta_y, delta_x) 

    angle_diff = angle_to_goal - robot_pose.pose.orientation.z
    while angle_diff > 0.05 or angle_diff < -0.05:
        twist_msg.angular.z = 0.5
        pub_twist.publish(twist_msg)
        angle_diff = angle_to_goal - robot_pose.pose.orientation.z
    twist_msg.angular.z = 0.0
    pub_twist.publish(twist_msg)

    distance = sqrt(delta_x**2 + delta_y**2)
    while not collision and distance > 0.1:
        twist_msg.linear.x = 0.7
        pub_twist.publish(twist_msg)
        delta_x = goal_pose.pose.position.x-robot_pose.pose.position.x
        delta_y = goal_pose.pose.position.y-robot_pose.pose.position.y
        distance = sqrt(delta_x**2 + delta_y**2)
    twist_msg.linear.x = 0.0
    pub_twist.publish(twist_msg)

    if collision:
        rospy.logwarn("Robot in collision!")
        continue

    angle_diff = goal_pose.pose.orientation.z - robot_pose.pose.orientation.z
    while angle_diff > 0.02 or angle_diff < -0.02: 
        twist_msg.angular.z = 0.5
        pub_twist.publish(twist_msg)
        angle_diff = goal_pose.pose.orientation.z - robot_pose.pose.orientation.z
    twist_msg.angular.z = 0.0
    pub_twist.publish(twist_msg)
    rospy.loginfo("Robot finished movement!")

