// Generated by gencpp from file lv3/Collision.msg
// DO NOT EDIT!


#ifndef LV3_MESSAGE_COLLISION_H
#define LV3_MESSAGE_COLLISION_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace lv3
{
template <class ContainerAllocator>
struct Collision_
{
  typedef Collision_<ContainerAllocator> Type;

  Collision_()
    : collision(false)
    , front_collision(false)  {
    }
  Collision_(const ContainerAllocator& _alloc)
    : collision(false)
    , front_collision(false)  {
  (void)_alloc;
    }



   typedef uint8_t _collision_type;
  _collision_type collision;

   typedef uint8_t _front_collision_type;
  _front_collision_type front_collision;





  typedef boost::shared_ptr< ::lv3::Collision_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::lv3::Collision_<ContainerAllocator> const> ConstPtr;

}; // struct Collision_

typedef ::lv3::Collision_<std::allocator<void> > Collision;

typedef boost::shared_ptr< ::lv3::Collision > CollisionPtr;
typedef boost::shared_ptr< ::lv3::Collision const> CollisionConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::lv3::Collision_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::lv3::Collision_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::lv3::Collision_<ContainerAllocator1> & lhs, const ::lv3::Collision_<ContainerAllocator2> & rhs)
{
  return lhs.collision == rhs.collision &&
    lhs.front_collision == rhs.front_collision;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::lv3::Collision_<ContainerAllocator1> & lhs, const ::lv3::Collision_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace lv3

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::lv3::Collision_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::lv3::Collision_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::lv3::Collision_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::lv3::Collision_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::lv3::Collision_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::lv3::Collision_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::lv3::Collision_<ContainerAllocator> >
{
  static const char* value()
  {
    return "21f4fa17c769c4e48cc80cb40cdaf791";
  }

  static const char* value(const ::lv3::Collision_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x21f4fa17c769c4e4ULL;
  static const uint64_t static_value2 = 0x8cc80cb40cdaf791ULL;
};

template<class ContainerAllocator>
struct DataType< ::lv3::Collision_<ContainerAllocator> >
{
  static const char* value()
  {
    return "lv3/Collision";
  }

  static const char* value(const ::lv3::Collision_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::lv3::Collision_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bool collision\n"
"bool front_collision\n"
;
  }

  static const char* value(const ::lv3::Collision_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::lv3::Collision_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.collision);
      stream.next(m.front_collision);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Collision_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::lv3::Collision_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::lv3::Collision_<ContainerAllocator>& v)
  {
    s << indent << "collision: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.collision);
    s << indent << "front_collision: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.front_collision);
  }
};

} // namespace message_operations
} // namespace ros

#endif // LV3_MESSAGE_COLLISION_H
