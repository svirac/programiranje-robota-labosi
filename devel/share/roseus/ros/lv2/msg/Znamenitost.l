;; Auto-generated. Do not edit!


(when (boundp 'lv2::Znamenitost)
  (if (not (find-package "LV2"))
    (make-package "LV2"))
  (shadow 'Znamenitost (find-package "LV2")))
(unless (find-package "LV2::ZNAMENITOST")
  (make-package "LV2::ZNAMENITOST"))

(in-package "ROS")
;;//! \htmlinclude Znamenitost.msg.html


(defclass lv2::Znamenitost
  :super ros::object
  :slots (_x _y _theta _name ))

(defmethod lv2::Znamenitost
  (:init
   (&key
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:theta __theta) 0.0)
    ((:name __name) "")
    )
   (send-super :init)
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _theta (float __theta))
   (setq _name (string __name))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:theta
   (&optional __theta)
   (if __theta (setq _theta __theta)) _theta)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:serialization-length
   ()
   (+
    ;; float32 _x
    4
    ;; float32 _y
    4
    ;; float32 _theta
    4
    ;; string _name
    4 (length _name)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _theta
       (sys::poke _theta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _theta
     (setq _theta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get lv2::Znamenitost :md5sum-) "57f001c49ab7b11d699f8606c1f4f7ff")
(setf (get lv2::Znamenitost :datatype-) "lv2/Znamenitost")
(setf (get lv2::Znamenitost :definition-)
      "float32 x
float32 y
float32 theta
string name
")



(provide :lv2/Znamenitost "57f001c49ab7b11d699f8606c1f4f7ff")


