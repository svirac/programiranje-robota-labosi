;; Auto-generated. Do not edit!


(when (boundp 'lv3::Collision)
  (if (not (find-package "LV3"))
    (make-package "LV3"))
  (shadow 'Collision (find-package "LV3")))
(unless (find-package "LV3::COLLISION")
  (make-package "LV3::COLLISION"))

(in-package "ROS")
;;//! \htmlinclude Collision.msg.html


(defclass lv3::Collision
  :super ros::object
  :slots (_collision _front_collision ))

(defmethod lv3::Collision
  (:init
   (&key
    ((:collision __collision) nil)
    ((:front_collision __front_collision) nil)
    )
   (send-super :init)
   (setq _collision __collision)
   (setq _front_collision __front_collision)
   self)
  (:collision
   (&optional __collision)
   (if __collision (setq _collision __collision)) _collision)
  (:front_collision
   (&optional __front_collision)
   (if __front_collision (setq _front_collision __front_collision)) _front_collision)
  (:serialization-length
   ()
   (+
    ;; bool _collision
    1
    ;; bool _front_collision
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _collision
       (if _collision (write-byte -1 s) (write-byte 0 s))
     ;; bool _front_collision
       (if _front_collision (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _collision
     (setq _collision (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _front_collision
     (setq _front_collision (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get lv3::Collision :md5sum-) "21f4fa17c769c4e48cc80cb40cdaf791")
(setf (get lv3::Collision :datatype-) "lv3/Collision")
(setf (get lv3::Collision :definition-)
      "bool collision
bool front_collision
")



(provide :lv3/Collision "21f4fa17c769c4e48cc80cb40cdaf791")


