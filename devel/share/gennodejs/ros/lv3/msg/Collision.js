// Auto-generated. Do not edit!

// (in-package lv3.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Collision {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.collision = null;
      this.front_collision = null;
    }
    else {
      if (initObj.hasOwnProperty('collision')) {
        this.collision = initObj.collision
      }
      else {
        this.collision = false;
      }
      if (initObj.hasOwnProperty('front_collision')) {
        this.front_collision = initObj.front_collision
      }
      else {
        this.front_collision = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Collision
    // Serialize message field [collision]
    bufferOffset = _serializer.bool(obj.collision, buffer, bufferOffset);
    // Serialize message field [front_collision]
    bufferOffset = _serializer.bool(obj.front_collision, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Collision
    let len;
    let data = new Collision(null);
    // Deserialize message field [collision]
    data.collision = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [front_collision]
    data.front_collision = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 2;
  }

  static datatype() {
    // Returns string type for a message object
    return 'lv3/Collision';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '21f4fa17c769c4e48cc80cb40cdaf791';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool collision
    bool front_collision
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Collision(null);
    if (msg.collision !== undefined) {
      resolved.collision = msg.collision;
    }
    else {
      resolved.collision = false
    }

    if (msg.front_collision !== undefined) {
      resolved.front_collision = msg.front_collision;
    }
    else {
      resolved.front_collision = false
    }

    return resolved;
    }
};

module.exports = Collision;
