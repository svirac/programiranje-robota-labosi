; Auto-generated. Do not edit!


(cl:in-package lv2-msg)


;//! \htmlinclude Znamenitost.msg.html

(cl:defclass <Znamenitost> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0)
   (theta
    :reader theta
    :initarg :theta
    :type cl:float
    :initform 0.0)
   (name
    :reader name
    :initarg :name
    :type cl:string
    :initform ""))
)

(cl:defclass Znamenitost (<Znamenitost>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Znamenitost>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Znamenitost)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lv2-msg:<Znamenitost> is deprecated: use lv2-msg:Znamenitost instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <Znamenitost>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lv2-msg:x-val is deprecated.  Use lv2-msg:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <Znamenitost>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lv2-msg:y-val is deprecated.  Use lv2-msg:y instead.")
  (y m))

(cl:ensure-generic-function 'theta-val :lambda-list '(m))
(cl:defmethod theta-val ((m <Znamenitost>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lv2-msg:theta-val is deprecated.  Use lv2-msg:theta instead.")
  (theta m))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <Znamenitost>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lv2-msg:name-val is deprecated.  Use lv2-msg:name instead.")
  (name m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Znamenitost>) ostream)
  "Serializes a message object of type '<Znamenitost>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'theta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Znamenitost>) istream)
  "Deserializes a message object of type '<Znamenitost>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'theta) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Znamenitost>)))
  "Returns string type for a message object of type '<Znamenitost>"
  "lv2/Znamenitost")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Znamenitost)))
  "Returns string type for a message object of type 'Znamenitost"
  "lv2/Znamenitost")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Znamenitost>)))
  "Returns md5sum for a message object of type '<Znamenitost>"
  "57f001c49ab7b11d699f8606c1f4f7ff")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Znamenitost)))
  "Returns md5sum for a message object of type 'Znamenitost"
  "57f001c49ab7b11d699f8606c1f4f7ff")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Znamenitost>)))
  "Returns full string definition for message of type '<Znamenitost>"
  (cl:format cl:nil "float32 x~%float32 y~%float32 theta~%string name~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Znamenitost)))
  "Returns full string definition for message of type 'Znamenitost"
  (cl:format cl:nil "float32 x~%float32 y~%float32 theta~%string name~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Znamenitost>))
  (cl:+ 0
     4
     4
     4
     4 (cl:length (cl:slot-value msg 'name))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Znamenitost>))
  "Converts a ROS message object to a list"
  (cl:list 'Znamenitost
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
    (cl:cons ':theta (theta msg))
    (cl:cons ':name (name msg))
))
