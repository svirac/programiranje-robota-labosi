
(cl:in-package :asdf)

(defsystem "lv2-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Znamenitost" :depends-on ("_package_Znamenitost"))
    (:file "_package_Znamenitost" :depends-on ("_package"))
  ))