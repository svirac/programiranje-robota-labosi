
(cl:in-package :asdf)

(defsystem "lv3-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Collision" :depends-on ("_package_Collision"))
    (:file "_package_Collision" :depends-on ("_package"))
  ))