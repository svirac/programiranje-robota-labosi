; Auto-generated. Do not edit!


(cl:in-package lv3-msg)


;//! \htmlinclude Collision.msg.html

(cl:defclass <Collision> (roslisp-msg-protocol:ros-message)
  ((collision
    :reader collision
    :initarg :collision
    :type cl:boolean
    :initform cl:nil)
   (front_collision
    :reader front_collision
    :initarg :front_collision
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Collision (<Collision>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Collision>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Collision)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name lv3-msg:<Collision> is deprecated: use lv3-msg:Collision instead.")))

(cl:ensure-generic-function 'collision-val :lambda-list '(m))
(cl:defmethod collision-val ((m <Collision>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lv3-msg:collision-val is deprecated.  Use lv3-msg:collision instead.")
  (collision m))

(cl:ensure-generic-function 'front_collision-val :lambda-list '(m))
(cl:defmethod front_collision-val ((m <Collision>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader lv3-msg:front_collision-val is deprecated.  Use lv3-msg:front_collision instead.")
  (front_collision m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Collision>) ostream)
  "Serializes a message object of type '<Collision>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'collision) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'front_collision) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Collision>) istream)
  "Deserializes a message object of type '<Collision>"
    (cl:setf (cl:slot-value msg 'collision) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'front_collision) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Collision>)))
  "Returns string type for a message object of type '<Collision>"
  "lv3/Collision")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Collision)))
  "Returns string type for a message object of type 'Collision"
  "lv3/Collision")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Collision>)))
  "Returns md5sum for a message object of type '<Collision>"
  "21f4fa17c769c4e48cc80cb40cdaf791")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Collision)))
  "Returns md5sum for a message object of type 'Collision"
  "21f4fa17c769c4e48cc80cb40cdaf791")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Collision>)))
  "Returns full string definition for message of type '<Collision>"
  (cl:format cl:nil "bool collision~%bool front_collision~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Collision)))
  "Returns full string definition for message of type 'Collision"
  (cl:format cl:nil "bool collision~%bool front_collision~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Collision>))
  (cl:+ 0
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Collision>))
  "Converts a ROS message object to a list"
  (cl:list 'Collision
    (cl:cons ':collision (collision msg))
    (cl:cons ':front_collision (front_collision msg))
))
