set(_CATKIN_CURRENT_PACKAGE "lv3")
set(lv3_VERSION "0.0.0")
set(lv3_MAINTAINER "user <user@todo.todo>")
set(lv3_PACKAGE_FORMAT "2")
set(lv3_BUILD_DEPENDS "roscpp" "rospy" "std_msgs" "message_generation")
set(lv3_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "std_msgs" "message_runtime")
set(lv3_BUILDTOOL_DEPENDS "catkin")
set(lv3_BUILDTOOL_EXPORT_DEPENDS )
set(lv3_EXEC_DEPENDS "roscpp" "rospy" "std_msgs" "message_runtime")
set(lv3_RUN_DEPENDS "roscpp" "rospy" "std_msgs" "message_runtime")
set(lv3_TEST_DEPENDS )
set(lv3_DOC_DEPENDS )
set(lv3_URL_WEBSITE "")
set(lv3_URL_BUGTRACKER "")
set(lv3_URL_REPOSITORY "")
set(lv3_DEPRECATED "")