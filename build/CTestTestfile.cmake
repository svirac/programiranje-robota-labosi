# CMake generated Testfile for 
# Source directory: /home/user/labosi_ws/src
# Build directory: /home/user/labosi_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("rob_manip_moveit_config")
subdirs("lv1")
subdirs("lv2")
subdirs("lv3")
subdirs("lv4")
subdirs("lv5")
